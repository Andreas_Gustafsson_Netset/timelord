import List from 'bff/list';

export default List.withProperties({
  totalExportableTime: {
    getter() {
      return this.reduce((sum, item) => sum + (item.task.isExportable ? item.duration : 0), 0);
    },
    dependencies: ['length', 'item:duration'],
  },

  totalNonexportableTime: {
    getter() {
      return this.reduce((sum, item) => sum + (item.task.isExportable ? 0 : item.duration), 0);
    },
    dependencies: ['length', 'item:duration'],
  },

  totalTime: {
    getter() {
      return this.totalExportableTime + this.totalNonexportableTime;
    },
    dependencies: ['totalExportableTime', 'totalNonexportableTime'],
  },
});
