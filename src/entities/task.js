import Record from 'bff/record';

import hasJiraPrefix from './util';

let serialNumber = 0;
const hashConstant = 0.5 * (Math.sqrt(5) - 1);
const maxAngle = 360;
const BASE_PDB_URL = 'https://projekt.netset.se/se/netset/company/web/I01.m4n?cmd=openTask&id=';
const BASE_JIRA_URL = 'https://ateaintern.atlassian.net/browse/ESHOP-';

const Task = Record.withProperties({
  id: {
    type: String,
    setter(taskId) {
      const id = String(taskId);
      if (hasJiraPrefix(id)) {
        return id.charAt(0).toUpperCase() + id.slice(1).replace(/\D/g, '');
      }

      if (Number.isInteger(Number(id))) {
        return id;
      }
      return -Math.floor(Math.random() * 1000000);
    },
  },
  name: {
    type: String,
  },
  lastUsed: {
    type: Number,
    // eslint-disable-next-line no-return-assign
    defaultValue: () => Date.now() - (serialNumber += 1),
  },
  isFavorite: {
    type: Boolean,
    defaultValue: false,
  },
  isLocked: {
    type: Boolean,
    defaultValue: false,
  },
  nReferences: {
    type: Number,
    defaultValue: 0,
  },
  colorAngle: {
    type: Number,
    setter: false,
    getter() {
      // Calculate a "hash" value in the range [0, maxAngle[
      // https://www.cs.hmc.edu/~geoff/classes/hmc.cs070.200101/homework10/hashfuncs.html
      const a = Math.abs(this.id.replace(/\D/g, '')) * hashConstant;
      const b = a % 1; // Get fractional part, range [0, 1[
      return Math.floor(b * maxAngle);
    },
  },
  isExportable: {
    type: Boolean,
    setter: false,
    getter() {
      if (hasJiraPrefix(this.id)) {
        return parseInt(this.id.slice(1), 10) >= 0;
      }
      return parseInt(this.id, 10) >= 0;
    },
  },
  taskUrl: {
    type: String,
    setter: false,
    getter() {
      const link = (hasJiraPrefix(this.id) ? BASE_JIRA_URL : BASE_PDB_URL) + this.id.replace(/\D/g, '');
      return link;
    },
  },
});
// eslint-disable-next-line func-names
Task.prototype.toJSON = function() {
  return {
    id: this.id,
    name: this.name,
    lastUsed: this.lastUsed,
    isFavorite: this.isFavorite,
    isLocked: this.isLocked,
  };
};

export default Task;
