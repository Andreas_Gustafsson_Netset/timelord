function hasJiraPrefix(taskId) {
  // move to util
  if (typeof taskId !== 'string') {
    return false;
  }
  return taskId.startsWith('J') || taskId.startsWith('j');
}

export default hasJiraPrefix;
