export default currentData => {
  const tasks = currentData.tasks;
  if (tasks !== undefined) {
    tasks.forEach(task => {
      // eslint-disable-next-line no-param-reassign
      task.id = String(task.id);
    });
  }
  const timeLineItems = currentData['timeline-items'];
  if (timeLineItems !== undefined) {
    timeLineItems.forEach(task => {
      // eslint-disable-next-line no-param-reassign
      task.taskId = String(task.taskId);
    });
  }

  return {
    ...currentData,
    version: 3,
  };
};
