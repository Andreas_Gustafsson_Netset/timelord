import migration1 from '../migrations/1';
import migration2 from '../migrations/2';

const baseData = {
  tasks: [
    { id: 37124, name: 'Breakfast', lastUsed: 1490707925941, isFavorite: true },
    { id: -3251652764263471, name: 'Lunch', lastUsed: 1490450679281, isFavorite: true },
    { id: 26949, name: 'Flex', lastUsed: 1490707940605, isFavorite: false },
    { id: 21805, name: 'Internal meeting', lastUsed: 1490707940474, isFavorite: false },
  ],
  'timeline-items': [
    { startTime: 850, endTime: 900, taskId: 21805, description: '' },
    { startTime: 900, endTime: 940, taskId: 26949, description: '' },
    { startTime: 940, endTime: 970, taskId: -3251652764263471, description: '' },
    { startTime: 970, endTime: 1030, taskId: 37124, description: '' },
    { startTime: 1030, endTime: 1075, taskId: 26949, description: 'heyflex' },
    { startTime: 1075, endTime: 1085, taskId: 37124, description: '' },
    { startTime: 1085, endTime: 1115, taskId: 21805, description: '' },
    { startTime: 1115, endTime: 1150, taskId: 21805, description: '' },
    { startTime: 1150, endTime: 1160, taskId: 21805, description: '' },
    { startTime: 1160, endTime: 1170, taskId: 21805, description: '' },
    { startTime: 1170, endTime: 1205, taskId: 26949, description: '' },
    { startTime: 1205, endTime: 1215, taskId: 26949, description: '' },
    { startTime: 1215, endTime: 1240, taskId: 26949, description: '' },
    { startTime: 1240, endTime: 1270, taskId: 26949, description: '' },
  ],
  version: 1,
};

const migrations = [baseData];

describe('migrations', () => {
  it('start with baseData', () => {
    expect(baseData).toMatchSnapshot();
  });
  it('migrate from version 1', () => {
    const preMigration = migrations[migrations.length - 1];
    const postMigration = migration1(preMigration);
    migrations.push(postMigration);
    expect(postMigration).toMatchSnapshot();
  });
  it('migrate from version 2', () => {
    const preMigration = migrations[migrations.length - 1];
    const postMigration = migration2(preMigration);
    migrations.push(postMigration);
    expect(postMigration).toMatchSnapshot();
  });
});
