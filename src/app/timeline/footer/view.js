import View from 'bff/view';

import time from 'env/time';

import template from './template.dot';

export default View.makeSubclass({
  constructor(items, timelineState) {
    this.items = items;
    this.timelineState = timelineState;

    this.render();

    this.listenTo(this.items, ['change:length', 'item:change'], this.requestRender);
    this.listenTo(this.timelineState, 'change', this.requestRender);

    this.listenTo('input.zoom-input', 'input', this.onZoomInput);
    this.listenTo('input.zoom-input', 'mousedown', this.onZoomMousedown);
  },

  template,

  minutesToTimeString: time.minutesToTimeString,

  onZoomInput(ev) {
    this.timelineState.zoom = parseFloat(ev.target.value, 10);
  },

  onZoomMousedown() {
    this.timelineState.isZooming = true;
    this.listenTo(document.body, 'mouseup', this.onMouseup);
  },

  onMouseup() {
    this.timelineState.isZooming = false;
    this.stopListening(document.body);
  },
});
