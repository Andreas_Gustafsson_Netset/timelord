import map from 'lodash/fp/map';
import max from 'lodash/fp/max';
import flow from 'lodash/fp/flow';

import padLeft from 'pad-left';
import Record from 'bff/record';
import View from 'bff/view';

import timelineItemsToAggregatedExportItems from './export-items';

import template from './template.dot';

const State = Record.withProperties({
  isClosing: { type: Boolean, defaultValue: false },
});

export default View.makeSubclass({
  constructor(items) {
    this.state = new State();
    this.items = items;

    this.render();

    this.listenTo(this.state, 'change', this.requestRender);
    this.listenTo('button.close', 'click', this.onCloseClicked);
    this.listenTo('.content-pane pre', 'click', this.onContentPaneClicked);

    document.querySelector('.app').appendChild(this.el);
  },

  template,

  itemsToPdbData(timelineItems) {
    const exportItems = timelineItemsToAggregatedExportItems(timelineItems);

    const maxDurationLength = flow(map(item => item.formatedDuration.length), max)(exportItems);
    const maxIdLength = flow(map(item => item.id.length), max)(exportItems);

    const exportLines = map(item => {
      const paddedId = padLeft(item.id, maxIdLength, ' ');
      const paddedDuration = padLeft(item.formatedDuration, maxDurationLength, ' ');
      return `${paddedId} | ${paddedDuration} | ${item.description}`;
    })(exportItems);

    return exportLines;
  },

  onCloseClicked() {
    this.listenTo(this.el, 'animationend', this.destroy);
    this.state.isClosing = true;
  },

  onContentPaneClicked(ev) {
    const selection = window.getSelection();
    if (selection.extentOffset !== selection.baseOffset) {
      return; // Only select all if nothing is currently selected
    }
    const range = document.createRange();
    range.selectNodeContents(ev.target);
    selection.removeAllRanges();
    selection.addRange(range);
  },
});
