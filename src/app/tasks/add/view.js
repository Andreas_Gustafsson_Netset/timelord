import Record from 'bff/record';
import View from 'bff/view';

import Task from 'entities/task';

import vent from 'env/vent';

import template from './template.dot';

const InputState = Record.withProperties({
  id: { type: String, defaultValue: '' },
  name: { type: String, defaultValue: '' },
  ctrl: { type: Boolean, defaultValue: false },
});

export default View.makeSubclass({
  constructor(tasks) {
    this.tasks = tasks;
    this.inputState = new InputState();

    this.render();

    this.listenTo(vent, 'request clear add task inputs', this.clearInputs);
    this.listenTo(this.el, 'keydown', this.onInputKeyDown);
    this.listenTo(document.body, ['keyup', 'keydown'], this.onKeyUpOrDown);
    this.listenTo('input', 'input', this.onInput);
    this.listenTo('.submit', 'click', this.createNewTask);
  },

  template,

  render() {
    View.prototype.render.call(this);
    this.$('.task-id').focus();
  },

  onInputKeyDown(ev) {
    switch (ev.key) {
      case 'Enter':
        this.createNewTask();
        break;
      case 'Escape':
        this.clearInputs();
        break;
    }
  },

  onKeyUpOrDown(ev) {
    switch (ev.key) {
      case 'Control':
        this.inputState.ctrl = { keyup: false, keydown: true }[ev.type];
        break;
    }
  },

  onInput() {
    this.inputState.id = this.$('input.task-id').value;
    this.inputState.name = this.$('input.task-name').value;
  },

  clearInputs() {
    this.$('input.task-id').value = '';
    this.$('input.task-name').value = '';
    this.onInput();
  },

  createNewTask() {
    const taskIdString = this.inputState.id;
    let taskId = taskIdString || undefined;
    const taskName = this.inputState.name;

    if ((taskId && !this.tasks.isValidExportableId(taskId)) || !(taskId || taskName)) {
      this.el.classList.remove('invalid-input-anim-trigger');
      this.forceRepaint();
      this.el.classList.add('invalid-input-anim-trigger');
      return;
    }

    if (!taskId) {
      taskId = this.tasks.getNextUniqueId(false);
    }

    if (!this.tasks.isTaskIdUnique(taskId)) {
      const task = this.tasks.find(t => t.id === taskId);
      task.emit('request highlight existing task', task);
      return;
    }

    const task = new Task({
      id: taskId,
      name: taskName,
    });
    this.tasks.push(task);

    this.clearInputs();

    if (this.inputState.ctrl) {
      vent.emit('request add task to timeline', task);
    }
  },
});
